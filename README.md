# boot_app library
This jar allows receive an API call to simulate a sms server printing the context in the logs using completable futures.

**POST http://localhost:8888/sms/server
body: List<Map<String,Object>>**

Note that spring's actuators **mappings**, **env**, **info**, **health**, **metrics**, **beans**, **autoconfig**, **trace**, **dump** and **configprops** are also enabled in the same port **8888**.