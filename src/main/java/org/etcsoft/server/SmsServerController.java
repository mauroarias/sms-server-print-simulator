package org.etcsoft.server;

import org.etcsoft.model.SmsEvent;
import org.etcsoft.service.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "sms/server")
public class SmsServerController {

    @Autowired
    private SmsService smsService;

    @RequestMapping(method = POST)
    ResponseEntity smsPosted(@RequestBody() List<SmsEvent> messages) {
        CompletableFuture.runAsync(() -> smsService.printMessages(messages));
        return new ResponseEntity(OK);
    }


}
