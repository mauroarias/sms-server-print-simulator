package org.etcsoft.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class SmsEvent
{
	private String createdOn;
	private String eventType;
	private String eventId;
	private Map<String, Object> bike;
	private Map<String, Object> trip;
	private Map<String, Object> customer;
	private Map<String, Object> product;
}
