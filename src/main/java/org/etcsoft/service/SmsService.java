package org.etcsoft.service;

import org.etcsoft.model.SmsEvent;

import java.util.List;

public interface SmsService {
    void printMessages(List<SmsEvent> messages);
}
