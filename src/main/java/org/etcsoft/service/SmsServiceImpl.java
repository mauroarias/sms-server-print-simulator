package org.etcsoft.service;

import org.etcsoft.model.SmsEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SmsServiceImpl implements SmsService {
    private static final Logger logger = LoggerFactory.getLogger(SmsServiceImpl.class);

    public void printMessages(List<SmsEvent> messages) {
        try {
			messages.forEach(message -> logger.info(message.toString()));
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }
}
